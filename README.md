Requirements
============

AI-toolbox from 14/01/17 https://github.com/Svalorzen/AI-Toolbox/commit/efd6589056081e923193524132017120950c04a2
CPLEX/CONCERT from IBM

Build Instructions
==================

Use "make test-sim". Requires to fill the firsts lines of the makefile with appropriate paths.

Launch Tests
============

There is only one command to launch tests:
./test-sim path_to_POMDP_file threshold n_steps n_runs initial_confidence exploration_constant sim_method gathering_method
Where:
- path_to_POMDP_file is a path to the POMDP file;
- threshold is the reward to obtain with a probability of at least initial_confidence;
- n_steps is the total number of steps of a run;
- n_runs is the number of runs;
- initial_confidence is the initial confidence;
- exploration_constant is POMCP's exploration constant; 
- sim_method corresponds to the simulation method: 1 for rollouts from the root and 0 for POMCP;
- gathering_method is the method used to study the exploration tree: 0 for a list based method, 1 for linear programming

This will output, step by step, the runs in the terminal. Four files are created:
- *.res: provides the result of a test (one command), in one line provides the input received, 
the average payoff of the test, and the observed probability to beat the threshold.
- *.time: provides, in second, the time of each step of each run.
- *.sum: 
- *.log: 

Structure of the code
=====================

The only file significantly modified is BWCPOMCP. Several functions are the same as in the original code.
Notable functions:
- sampleAction: initialises or truncate the exploration tree, calls runSimulation;
- runSimulation: launches all the POMCP simulations, for each that beat the threshold launches an exact simulation, 
then builds the lists and picks the best action; 
- simulatePOMCP: identical to the former simulate;
- simulateExact: given the trace of a simulation, compute the exact belief states and probabilities along the path.
- *Gather: functions used to gather informations from the exploration tree.
- linearSolving* : functions used to gather informations from the exploration tree with linear programming.

