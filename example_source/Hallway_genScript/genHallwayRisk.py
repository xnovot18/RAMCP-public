__author__ = 'pnovotny. based on an earlier script by mchmelik', 

import sys
import itertools
import copy

POMDP = True # switch to True to generate POMDPs instead of MDPs

# Generates a (PO)MDP file with a Hallway instance from a given maze map in a text file.
# Usage: python genHallway.py input_map.txt output_file [discount_factor]

# UPDATE: This is a version suitable for generating benchmarks for probabilistic threshold optimization.

# Hallway: a classic POMDP robot navigation benchmark. We have a maze with walls, traps and goals.
# The robot can move forward or turn left and right and it can only sense walls around itself (it cannot sense
# traps). The goal is to find the target as fast as possible (i.e. actions have zero reward until
# target is reached, after which a large number is recieved, discounted by the number of steps).

# The maze is given as a text file, each row representing a row of a maze,
# individual cells in a row separated by spaces. See the following example:

# USAGE: python genHallway.py infile outfile [discount| def=0.98] [bump or destroy on wall | 1=bump (default) 0=destroy]

# In this version we no longer have a goal POMDP. Instead, there are tasks ti finish.
# Some are easier to finish, some are riskier. For a finished task we get a reward,
# but if it fails, we get a penalty. Task risk is determine by a number 2--9.
# The riskier the task, the larger the average reward but the larger penalty  or larger risk of
# failure.

# 1 1 1 1 1 1 
# 1 f + + 0 1 
# 1 1 + + 1 1 
# 1 0 0 0 7 1 
# 1 1 2 x 1 1 
# 1 1 1 1 1 1 

# 1s are walls, 0s are empty cells, R are reloading places (this is from a different application domain,
# we probably don't need this), x is a spinning trap, f is a location where there is, on each passing, a possibility of loss (not yet implemented), 2--9 are various tasks.
# (state with a self-loop), + is a possible starting location. The robot starts in a starting location
# chosen uniformly at random, looking southwards.

# FOR THE SCRIPT TO FUNCTION, THE MAZE HAS TO BE SURROUNDED BY WALLS

# Traps spin the guy around and thus add PO features.

# Safe probability of tasks, from safest to riskiest.

TASK = {"2","3","4","5","6","7","8","9"}

unSafeProbs = [0,0.1,0.5,0.8,0.85,1,1,0.5]

# Rewards of tasks, from safest to riskiest.
taskRewards = [100,2300,3500,12000,20000,0,0,10]


# Penalties of tasks, from safest to riskiest.

taskPenalties = [0,0,0,0,0,-2000,-1500,0]

persistentTrapUnSafeProb = 0.15

persistentTrapPenalty = -20000

states = []
groundStates = []
taskStates = []
trapStates = []
initialStates = []
transitions = []

def taskStr(lst):
	#print(lst)
	out = ""
	for elem in lst:
		if elem:
			out = out + "m"
		else:
			out = out + "u"
		#print(out)
	return out

class State:

	def __init__(self,x,y,cell,orient):
		self.x = x
		self.y = y
		self.cell = cell
		self.orient = orient
		self.tasks = []
		self.badFlag = ""
		self.rewardOut = 0.0

	def setTasks(self,tasks):
		self.tasks = list(tasks)
		#print(self.tasks)

	def outputString(self):
		taskString = taskStr(self.tasks)
		return "x" + str(self.x) + "y" + str(self.y) + "h" + self.orient + self.badFlag + "t" + ''.join(taskString)

	def getFwdSuccessor(self):
		newx = self.x + changeMap[self.orient][0]
		newy = self.y + changeMap[self.orient][1]
		newCell = maze[newx][newy]
		succ = copy.deepcopy(self)
		succ.x = newx
		succ.y = newy
		succ.cell = newCell
		return succ

	def getLeftSuccessor(self):
		succ = State(self.x,self.y,self.cell,left[self.orient])
		succ.tasks = self.tasks
		return succ

	def getRightSuccessor(self):
		succ = State(self.x,self.y,self.cell,right[self.orient])
		succ.tasks = self.tasks
		return succ

	def setOrient(self,orient):
		self.orient = orient

	def getObservation(self):
		if POMDP:
			if self.badFlag == "":
				obs = "o"+''.join(getObs(self.x,self.y,self.orient))
			else:
				obs = "o" + self.cell + self.badFlag + ''.join(getObs(self.x,self.y,self.orient))
		else:
			obs = "ox" + str(self.x) + "y" + str(self.y) + self.badFlag + ''.join(getObs(self.x,self.y,self.orient))
		return obs

	def isTask(self):
		succ = self.getFwdSuccessor()
		return ((succ.cell in TASK) and (not self.tasks[taskDict[(succ.x,succ.y)]]))
		#return self.cell in {"2","3","4","5","6","7","8","9"}

	def badFlagOn(self):
		self.badFlag = "b"

	def isInitTask(self):
		ret = True
		for t in self.tasks:
			if t == True:
				ret = False
		return ret


class Transition:

	def __init__(self,source,action,target,prob,rew):
		self.source = source
		self.target = target
		self.action = action
		self.prob = prob
		self.rew = rew






if len(sys.argv)>3:
	discount = sys.argv[3]
else:
	discount="0.95"

if len(sys.argv)>4:
    bump= sys.argv[4]
else:
    bump= 1

# How does the orientation change when turning around.

left = {'N':'W','W':'S','S':'E','E':'N'}
right = {'N':'E','E':'S','S':'W','W':'N'}

# Coordinate change after moving forward with a given orientation.

changeMap = {'N':[-1,0],'S':[1,0],'E':[0,1],'W':[0,-1]}

def isWall(i,j,direction):
    if(direction.__eq__('N')):
        return (maze[i-1][j] == '1')
    if(direction.__eq__('E')):
        return (maze[i][j+1] == '1')
    if(direction.__eq__('W')):
        return (maze[i][j-1] == '1')
    if(direction.__eq__('S')):
        return (maze[i+1][j] == '1')

def isTrap(i,j,direction):
    if(direction.__eq__('N')):
        return (maze[i-1][j] == 'x')
    if(direction.__eq__('E')):
        return (maze[i][j+1] == 'x')
    if(direction.__eq__('W')):
        return (maze[i][j-1] == 'x')
    if(direction.__eq__('S')):
        return (maze[i+1][j] == 'x')

def isfTrap(i,j,direction):
    if(direction.__eq__('N')):
        return (maze[i-1][j] == 'f')
    if(direction.__eq__('E')):
        return (maze[i][j+1] == 'f')
    if(direction.__eq__('W')):
        return (maze[i][j-1] == 'f')
    if(direction.__eq__('S')):
        return (maze[i+1][j] == 'f')

def getObs(i,j,orientation):
    if(orientation.__eq__('N')):
        return ['F' if isWall(i,j,'N') else 'f', 'L' if isWall(i,j,'W') else 'l', 'R' if isWall(i,j,'E') else 'r', 'B' if isWall(i,j,'S') else 'b']
    if(orientation.__eq__('E')):
        return ['F' if isWall(i,j,'E') else 'f', 'L' if isWall(i,j,'N') else 'l', 'R' if isWall(i,j,'S') else 'r', 'B' if isWall(i,j,'W') else 'b']
    if(orientation.__eq__('S')):
        return ['F' if isWall(i,j,'S') else 'f', 'L' if isWall(i,j,'E') else 'l', 'R' if isWall(i,j,'W') else 'r', 'B' if isWall(i,j,'N') else 'b']
    if(orientation.__eq__('W')):
        return ['F' if isWall(i,j,'W') else 'f', 'L' if isWall(i,j,'S') else 'l', 'R' if isWall(i,j,'N') else 'r', 'B' if isWall(i,j,'E') else 'b']

# Retrieve a new orientation after a turn is made.

def turnLeft(orient):
    return left[orient]

def turnRight(orient):
    return right[orient]

fIn = open(sys.argv[1], 'r');

maze = [];
for line in fIn.readlines():
    #lineField = line.split()
    maze.append(line.split());

width = len(maze[0]);
height = len(maze);

taskNum = 0

taskDict = {}

# Set up underlying maze states

for i in range(0, height):
    for j in range(0, width):
        element = maze[i][j]
        if element in {"0","+","g","x","f","R","2","3","4","5","6","7","8","9"}:
        	for orient in {'N','E','S','W'}:
        		groundStates.append(State(i,j,element,orient))
        	if element in TASK:
        		taskDict[(i,j)] = taskNum
        		taskNum = taskNum + 1
        	#	taskStates.append(State(i,j,element,orient))
        	#if element == "x":
        	#	trapStates.append(State(i,j,element,orient))

taskList = list(itertools.product([True,False], repeat = taskNum))
#for a in taskList:
#	print(a)
# Set up states with task status lists

for groundState in groundStates:
	for taskTuple in taskList:
		state = State(groundState.x,groundState.y,groundState.cell,groundState.orient)
		state.setTasks(taskTuple)
		states.append(state)

del taskList
del groundStates

stateCache = [] 

#for state in states:


# Generate transitions and initial states:
for state in states:
	if isWall(state.x,state.y,state.orient):
		transition = Transition(state,"f",state,1.0,0.0)
		transitions.append(transition)
	elif isTrap(state.x,state.y,state.orient):
		nextState = state.getFwdSuccessor()
		for newOr in ["N","E","W","S"]:
			if newOr == state.orient:
				hprob = 0.85
			else:
				hprob = 0.05
			nextState.setOrient(newOr)
			newState = copy.deepcopy(nextState)
			transition = Transition(state,"f",newState,hprob,0.0)
			transitions.append(transition)
	elif isfTrap(state.x,state.y,state.orient):
		nextState = state.getFwdSuccessor()
		newState = copy.deepcopy(nextState)
		newState = copy.deepcopy(nextState)
		newState.badFlag = "g"
		prob = 1.0 - persistentTrapUnSafeProb
		transition = Transition(state,"f",newState,prob,0.0)
		transitions.append(transition)
		stateCache.append(newState)

		prob = persistentTrapUnSafeProb
		rew = persistentTrapPenalty
		badState = copy.deepcopy(nextState)
		badState.badFlagOn()
		badState.rewardOut = rew
		stateCache.append(badState)
		transition = Transition(state,"f",badState,prob,0.0)
		transitions.append(transition)

		for act in {"f","l","r"}:
			transition = Transition(badState,act,nextState,1.0,0.0)
			transitions.append(transition)
			transition = Transition(newState,act,nextState,1.0,0.0)
			transitions.append(transition)

	elif state.isTask():
		nextState = state.getFwdSuccessor()
		taskType = int(nextState.cell)-2
		#print(taskType)
		#print(taskDict[(nextState.x,nextState.y)])
		(nextState.tasks)[taskDict[(nextState.x,nextState.y)]] = True
		newState = copy.deepcopy(nextState)
		rew = taskRewards[taskType]
		newState.badFlag = "g"
		newState.rewardOut = rew
		prob = 1.0 - unSafeProbs[taskType]
		transition = Transition(state,"f",newState,prob,0.0)
		transitions.append(transition)
		stateCache.append(newState)

		prob = unSafeProbs[taskType]
		rew = taskPenalties[taskType]
		badState = copy.deepcopy(nextState)
		badState.badFlagOn()
		badState.rewardOut = rew
		stateCache.append(badState)
		transition = Transition(state,"f",badState,prob,0.0)
		transitions.append(transition)
		for act in {"f","l","r"}:
			transition = Transition(badState,act,nextState,1.0,0.0)
			transitions.append(transition)
			transition = Transition(newState,act,nextState,1.0,0.0)
			transitions.append(transition)

	else:
		nextState = state.getFwdSuccessor()
		transition = Transition(state,"f",nextState,1.0,0.0)
		transitions.append(transition)

	newState = state.getLeftSuccessor()
	transition = Transition(state,"l",newState,1.0,0.0)
	transitions.append(transition)

	newState = state.getRightSuccessor()
	transition = Transition(state,"r",newState,1.0,0.0)
	transitions.append(transition)

	if ((state.cell == "+") and (state.orient == "S") and state.isInitTask()):
		initialStates.append(state)

states = states + stateCache

# generate observations and obs function
observations = set()
stateToObsDict = {}

for state in states:
	obs = state.getObservation()
	observations.add(obs)
	stateToObsDict[state.outputString()] = obs
	#print(obs)



# output

fOut = open(sys.argv[2], 'w')
fOut.write("discount: "+discount+"\n")
fOut.write("values: reward\n")
fOut.write("states: ")

for state in states:
	fOut.write(state.outputString() + ' ')
fOut.write("\n\n")
fOut.write("actions: f l r \n\n")
fOut.write("observations: ")
for obs in observations:
	fOut.write(obs + ' ')
fOut.write("\n\n")
fOut.write("start include: ")
for state in initialStates:
	fOut.write(state.outputString() + ' ')
fOut.write("\n\n")

for transition in transitions:
	fOut.write("T: %s : %s : %s  %f\n" % (transition.action,transition.source.outputString(),transition.target.outputString(),transition.prob ) )
fOut.write("\n\n")

for state in states:
	fOut.write("O: * : % s : % s 1.0\n" % (state.outputString(),stateToObsDict[state.outputString()])  )
fOut.write("\n\n")

fOut.write("R: * : * : * : * 0.0\n")

for state in states:
	if not state.badFlag == "":
		fOut.write("R: * : " + state.outputString() + " : * : * " + str(state.rewardOut)+ "\n")

print("Num of states: " + str(len(states)))
print("Width", width);
print("Heigth", height);
print("Succesfully generated a POMDP format file.")






# WHAT REMAINS TO BE IMPLEMENTED:
# STATE CLASS MUST HAVE A METHOD TO CHANGE TASK LIST (OR WE CHANGE IT DIRECTLY)
# THIS FUNCTIONALITY NEEDS TO BE USED WHEN GENERATING TRANSITIONS FOR GOING INTO TASK STATES
# MAYBE IMPLEMENT GOAL STATES AND SINK FROM ALL TASK MINED STATES
# SETTING UP OF INITIAL STATES IS NOW BROKEN, AS ONLY STATES WITH INITIAL TASK LIST FULL OF ONES
# CAN BE INITIAL


















