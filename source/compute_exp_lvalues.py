from __future__ import division

__author__ = 'pnovotny'


import sys
import csv
import numpy



# USAGE compute_exp_lvalues.py logfile 

fileName = sys.argv[1]

gather = []
results = []
file = open(fileName,'rb')
#reader = csv.reader(file,dialect = 'excel')
for line in file:
	lineField = line.split(',')
	if len(lineField)>1:
		lval = float((lineField[0].split())[1])
		gather.append(lval)
	#gather.append(float(line[0]))

file.close()

gather.sort()

lValAvg = numpy.mean(gather)

curVal = float("-inf")

length=len(gather)

i=0

for val in gather:
	if val == curVal:
		i=i+1
		continue
	else:
		curVal = val
		prob = (length - i)/length
		results.append([val,prob])
		i = i+ 1
	


outFileName = fileName[:-4]
outFileName = outFileName + ".lval"

outFile = open(outFileName,'wb')

outFile.write("Average LValue in the first step is: " + str(lValAvg) + "\n\n")

writer = csv.writer(outFile)
for item in results:
	writer.writerow(item)

outFile.close()






