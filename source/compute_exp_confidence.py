from __future__ import division

__author__ = 'pnovotny'


import sys
import csv


# USAGE compute_exp_confidence.py resfile 

fileName = sys.argv[1]

gather = []
results = []
file = open(fileName,'rb')
reader = csv.reader(file,dialect = 'excel')
for line in reader:
	gather.append(float(line[0]))

file.close()

gather.sort()

curVal = float("-inf")

length=len(gather)

i=0

for val in gather:
	if val == curVal:
		i=i+1
		continue
	else:
		curVal = val
		prob = (length - i)/length
		results.append([val,prob])
		i=i+1
	#i = i+1


outFileName = fileName[:-4]
outFileName = outFileName + ".conf"

outFile = open(outFileName,'wb')

writer = csv.writer(outFile)
for item in results:
	writer.writerow(item)

outFile.close()






