#include <assert.h>
#include <iostream>
#include<string>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <AIToolbox/POMDP/Algorithms/POMCP.hpp>
#include <AIToolbox/POMDP/Types.hpp>
#include <AIToolbox/POMDP/Utils.hpp>
//#include <Python.h>


#include "pomdp.h"
#include "BWCPOMCP.hpp"


int main (int argc, char *argv[]) {
    if (argc != 9) {
        std::cerr << "Missing argument"<<  std::endl;
        std::cerr << "Call format:" << std::endl;
        std::cerr << argv[0] << " filename bound horizon n_tests initial_conf exploration_const sim_method analyse_method [lp=1]"<< std::endl;
        exit(1);
    }
    // recover the POMDP from file and parse horizon
    bool was_timeout = false;
    const float threshold = std::atof(argv[2]);
    const long max_timestep = std::atoi(argv[3]);
    long n_tests = std::atoi(argv[4]);
    const double conf = std::atof(argv[5]);
    const double exp = std::atof(argv[6]);
    const size_t sm = std::atoi(argv[7]);
    const size_t lp = std::atoi(argv[8]);
    auto inputPos = std::string((argv[1])).rfind('/');
    std::string inFileName = std::string(argv[1]);
    if (inputPos != std::string::npos) { inFileName.erase(0,inputPos+1); }
    const std::string resFileName = inFileName + "_hor_" + argv[3] + "_sims_" + argv[4] + "_thr_" + argv[2] + "_conf_" + argv[5] + "_ec_" + argv[6] + "_lp_" + argv[8];
    const std::string timeFileName = resFileName + ".time";
    const std::string sumFileName = resFileName + ".sum";
    const std::string resultFileName = resFileName + ".res";
    const std::string outputFileName = resFileName + ".out";
    const std::string logFileName = resFileName + ".log";

    BWC::POMDP M(argv[1]);
    // check some of its properties to assert it is a valid MDP
    // M.print(std::cout);
    //assert(M.isValidMdp());
    //assert(M.hasObsWeights());
    // obtain the initial belief
    AIToolbox::POMDP::Belief initial_belief = M.getInitialBelief();
    // obtain the worst-case value of the belief game
    //std::cout << "Solving the game" << std::endl;
    //BWC::POMDP N(M);
    //N.solveGameBeliefConstruction();
    //std::cout << "Done solving the game" << std::endl;
    // make the model in which we will simulate playing
    std::cout << "Start building AI-Toolbox model" << std::endl;
    auto model = M.makeSparseModel();
    
    // simulate the game
    
		//AIToolbox::POMDP::POMCP<decltype(model)> solver2(model,1000,1000,5);
    
    float average_reward = 0;
    float succeded = 0;
    for (long i_test = 0; i_test < n_tests; i_test++) {
        // create the model solver
        AIToolbox::POMDP::BWCPOMCP<decltype(model)> solver(
            model,
            1000,         // size of initial particle belief
            100000,        // number of episodes to run before completion
            conf,                   // aimed confidence
            threshold,    // the worst-case threshold
            exp,                    // the exploration constant
            sm,                     // the simaulation method
            lp,                     // use lp?
            &M,
            timeFileName // file into which I want to write the time durations
            );          // reference to belief-based game
        solver.setConfidence(conf);
        AIToolbox::POMDP::Belief new_belief, current_belief;
        size_t current_state, new_state, action, current_obs, new_obs;
        float reward;
        float total_reward = 0;
        float disc = M.getDiscFactor();
        current_belief = initial_belief;
        current_state = M.sampleInitialState();
        current_obs = -1;
	// sampleAction called with the following signature (i.e. without specifying observation)
	// resets the internal graph
        action = solver.sampleAction(current_belief,
                                     max_timestep); // horizon to plan for
        for (unsigned timestep = 0; timestep < max_timestep; ++timestep) {
                // if (solver.timeout_occured) {
                //     was_timeout = true;
                //     n_tests = 10;
                // }
	          std::tie(new_state, new_obs, reward) =
	              model.sampleSOR(current_state, action);
                std::ofstream logFile (logFileName, std::ios::app); 
                if (logFile.is_open()) {
                        logFile << "LValue: " << solver.valL(solver.getGraph()) <<", CurState: " << M.getStateName(current_state)<<", Action: "
                         << M.getActionName(action) <<", NewState: " << M.getStateName(new_state) << ", Iters: " << solver.k_ << ";;" ;
                        logFile.close();
                }   else std::cout << "Unable to open outFile";         
	          
	          std::cout << "played action " << M.getActionName(action)
	                    << " from state " << M.getStateName(current_state)
	                    << " and received state, obs, reward = "
	                    << M.getStateName(new_state)
	                    << ", " << M.getObsName(new_obs) << ", "
	                    << reward << std::endl;
	          new_belief = AIToolbox::POMDP::updateBeliefUnnormalized(model,
	                                                                  current_belief,
	                                                                  action, new_obs);
	          total_reward += std::pow(disc, timestep) * reward;
	          current_state = new_state;
	          current_obs = new_obs;
	          current_belief = new_belief;
	          action = solver.sampleAction(action, current_obs,
	                                       max_timestep - (timestep + 1));
	      }
        std::ofstream timeFile (timeFileName, std::ios::app);
        if (timeFile.is_open()) {
						timeFile << std::endl << std::endl;
						timeFile.close();
				}	else std::cout << "Unable to open outFile";
        std::ofstream logFile (logFileName, std::ios::app); 
                if (logFile.is_open()) {
                        logFile << std::endl << std::endl;
                        logFile.close();
                }   else std::cout << "Unable to open outFile";
        std::cout << "Obtained an accum reward of: " << total_reward << std::endl;
        average_reward += total_reward;
	// resultFileName = "RES_"+argv[1]+"th"+str(threshold)+
        std::ofstream resFile (resultFileName, std::ios::app);
        if (resFile.is_open()) {
					resFile << total_reward << ", ";
					if(threshold<=total_reward){
						resFile << 1;
					} else {
						resFile << 0;
					}
					
					resFile << "\n";
					resFile.close();
				}
				else std::cout << "Unable to open result.txt";
        if (threshold<=total_reward) {succeded += 1;}
    }
    average_reward = average_reward / n_tests;
    succeded = succeded / n_tests;
    std::cout << "Obtained, in average, an accum reward of: " << average_reward << std::endl;

  	std::ifstream fb (sumFileName, std::ifstream::in);
  	bool emptyFile;
	  emptyFile = (fb.peek() == std::ifstream::traits_type::eof());
  	
		std::ofstream outFile (sumFileName, std::ios::app);
		if (outFile.is_open()) {
			if(emptyFile) {
				outFile  << "Threshold" 				
				<< ", " << "Horizon" 							
				<< ", " << "Number_of_executions"	
				<< ", " << "Initial_confidence"		
				<< ", " << "Exploration_constant"	
				<< ", " << "Average_reward" 			
				<< ", " << "Percentage_of_success\n";
			}
			outFile /*<< "file: " << argv[1] 
			<< ", "*/ << std::atof(argv[2]) //t
			<< ", " << std::atof(argv[3]) 	//max number of steps
			<< ", " << std::atof(argv[4])		//ntests
			<< ", " << conf									//conf
			<< ", " << exp									//exp
			<< ", " << average_reward 			//average_reward
			<< ", " << succeded							//sucess rate
			<<"\n" ;
			outFile.close();
		}
		else std::cout << "Unable to open outFile";
    std::ofstream logFile (logFileName, std::ios::app);
    if (!was_timeout) {
        logFile << "Test finished without issues.\n";
        logFile.close();
        exit(EXIT_SUCCESS);
    }
    else {
        logFile << "2: Could not get over the confidence threshold in 60 secs of simulation, reduced number of execs to 10.\n";
        logFile.close();
        exit(EXIT_SUCCESS);
    }
}
