__author__ = 'pnovotny'

import sys
import subprocess
import csv
import numpy
import os
import time

inName = sys.argv[1]

file = open(inName,'r')
curDateTime = time.strftime("%y_%m_%d_%H:%M")
fileName = inName + curDateTime

for line in file:
	command = line.split()
	if len(command) == 8:
		print(command)
		ret = subprocess.call(["./test-sim"]+command)
		if not ret == 0:
			print("Something is wrong with the call " + line + ", bad arguments \n")
			continue
		else:	
			outFileName = (command[0].split('/'))[-1]
			baseFileName = outFileName + "_hor_" + command[2] + "_sims_" + command[3] + "_thr_" + command[1] + "_conf_" + command[4] + "_ec_" + command[5] + "_lp_" + command[7]
			logFileName = baseFileName + ".log"
			logFile = open(logFileName, 'r')
			logline = logFile.readline()
			logFile.close()
			resFileName = baseFileName + ".res"
			timeFileName = baseFileName + ".time"
			sumFileName = baseFileName + ".sum"
			if logline[0] == '2':
				baseFileNameTwo = outFileName + "_hor_" + command[2] + "_sims_" + "10" + "_thr_" + command[1] + "_conf_" + command[4] + "_ec_" + command[5] + "_lp_" + command[7]
				subprocess.call(["mv",resFileName,baseFileNameTwo+".res"])
				subprocess.call(["mv",timeFileName,baseFileNameTwo+".time"])
				subprocess.call(["mv",sumFileName,baseFileNameTwo+".sum"])
				subprocess.call(["mv",logFileName,baseFileNameTwo+".log"])
				baseFileName = baseFileNameTwo
				logFileName = baseFileName + ".log"
				resFileName = baseFileName + ".res"
				timeFileName = baseFileName + ".time"
				sumFileName = baseFileName + ".sum"
			resFile = open(resFileName,'rb')
			reader = csv.reader(resFile,dialect = 'excel')
			rewards = []
			successes = []
			for line in reader:
				floatLine = map(float,line)
				rewards.append(floatLine[0])
				successes.append(floatLine[1])
			if len(rewards) == 0:
				print("Something is wrong with the call " + line + ", no runs were recorded\n")
				continue
			resFile.close()
			stddevrew = numpy.std(rewards)
			stddevsuc = numpy.std(successes)
			sumFile = open(sumFileName,'r')
			fl = sumFile.readline()
			sl = sumFile.readline()
			sumFile.close()
			fl = fl[0:-1] + ", RewStdDev, SuccStdDev"
			sl = sl[0:-1] + ", " + str(stddevrew) + ", " + str(stddevsuc)
			sumFile = open(sumFileName,'w')
			sumFile.write(fl + "\n")
			sumFile.write(sl + "\n")
			sumFile.close()
			if not os.path.isdir("./test_results/"+fileName):
				subprocess.call(["mkdir","./test_results/"+fileName])
			subprocess.call(["mv",resFileName,"./test_results/"+fileName])
			subprocess.call(["mv",timeFileName,"./test_results/"+fileName])
			subprocess.call(["mv",sumFileName,"./test_results/"+fileName])
			subprocess.call(["mv",logFileName,"./test_results/"+fileName])
			subprocess.call(["python","compute_exp_lvalues.py","./test_results/"+fileName+"/"+logFileName])
			subprocess.call(["python","compute_exp_confidence.py","./test_results/"+fileName+"/"+resFileName])

# subprocess.call(["cp",fileName,"./test_results/"+fileName])

# A SUBPROCEDURE TO COMPUTE CSV OF OUTPUT

csvFileName = fileName + ".csv"
csvFile = open(csvFileName,'wb')
writer = csv.writer(csvFile)
writer.writerow(["Risk bound","Avg. payoff","Empirical risk","Stated risk"])

for fn in os.listdir("./test_results/"+fileName):
    if fn.endswith(".sum"):
    	sf = open("./test_results/"+fileName+"/"+fn,'rb')
    	reader = csv.reader(sf,dialect = 'excel')
    	next(reader)
    	row = reader.next()
    	newrow = [row[3],row[5],row[6]]
    	sf.close()
    	lf = open("./test_results/"+fileName+"/"+(fn[:-4])+".lval",'r')
    	line = lf.readline()
    	newrow.append((line.split())[-1])
    	writer.writerow(newrow)
    	lf.close()

csvFile.close()





