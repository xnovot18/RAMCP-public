#ifndef AI_TOOLBOX_POMDP_BWCPOMCP_HEADER_FILE
#define AI_TOOLBOX_POMDP_BWCPOMCP_HEADER_FILE

#include <unordered_map>
#include <iostream>
#include <math.h>
#include "pomdp.h"
#include <time.h>
#include <boost/timer/timer.hpp> 
#include <boost/format.hpp>
#include <fstream> 
#include <queue>
#include <list>
#include <random>
#include <ostream> 
#include <iostream>
#include <chrono>
#include <string>


#include <AIToolbox/POMDP/Types.hpp>
#include <AIToolbox/POMDP/SparseModel.hpp>
#include <AIToolbox/Utils/Probability.hpp>
#include <AIToolbox/Impl/Seeder.hpp>

#include <ilcplex/ilocplex.h>

#define TRUNC 0 // if enabled the probabilities are truncated wrt the value of MAGNITUDE
#define MAGNITUDE 7
#define ALL_EXACT_UPDATES 0 // if enabled, the exact probabilities are computed for each simulations
#define LOOP_WHILE 1 // the simulations will go on until a path with more than the minimal confidence is found
#define CEPS 0.0001 

namespace AIToolbox {
    namespace POMDP {

#ifndef DOXYGEN_SKIP
        // This is done to avoid bringing around the enable_if everywhere.
        template <typename M, typename = typename std::enable_if<is_generative_model<M>::value>::type>
        class BWCPOMCP;
#endif

        /**
         * @brief This class represents the BWCPOMCP online planner using UCB1.
         *
         * This algorithm is an online planner for POMDPs. As an online
         * planner, it needs to have a generative model of the problem. This
         * means that it only needs a way to sample transitions, observations
         * and rewards from the model, but it does not need to know directly
         * the distribution probabilities for them.
         *
         * BWCPOMCP plans for a single belief at a time. It follows the logic of
         * Monte Carlo Tree Sampling, where a tree structure is build
         * progressively and action values are deduced as averages of the
         * obtained rewards over rollouts. If the number of sample episodes is
         * high enough, it is guaranteed to converge to the optimal solution.
         *
         * At each rollout, we follow each action and observation within the
         * tree from root to leaves. During this path we chose actions using an
         * algorithm called UCT. What this does is privilege the most promising
         * actions, while guaranteeing that in the limit every action will still
         * be tried an infinite amount of times.
         *
         * Once we arrive to a leaf in the tree, we then expand it with a
         * single new node, representing a new observation we just collected.
         * We then proceed outside the tree following a random policy, but this
         * time we do not track which actions and observations we actually
         * take/obtain. The final reward obtained by this random rollout policy
         * is used to approximate the values for all nodes visited in this
         * rollout inside the tree, before leaving it.
         *
         * Since BWCPOMCP expands a tree, it can reuse work it has done if
         * multiple action requests are done in order. To do so, it simply asks
         * for the action that has been performed and its respective obtained
         * observation. Then it simply makes that root branch the new root, and
         * starts again.
         *
         * In order to avoid performing belief updates between each
         * action/observation pair, which can be expensive, BWCPOMCP uses particle
         * beliefs. These approximate the beliefs at every step, and are used
         * to select states in the rollouts.
         *
         * A weakness of this implementation is that, as every particle
         * approximation of continuous values, it will lose particles in time.
         * To fight this a possibility is to implement a particle
         * reinvigoration method, which would introduce noise in the particle
         * beliefs in order to keep them "fresh" (possibly using domain
         * knowledge).
         */

        template <typename M>
        class BWCPOMCP<M> {
            public:
		// in-tree beliefs are basically lists of states
                using SampleBelief = std::vector<size_t>;
                

                struct BeliefNode;
		// map from observations to succesor belief states
                using BeliefNodes = std::map<size_t, BeliefNode>;

                struct ActionNode {
                    BeliefNodes children;
                    //std::map<size_t,size_t> observationDistribution;
                    double V = 0.0;
                    double U = 1.0;
                    double L = 0.0;
                    unsigned N = 0;
                    unsigned oN = 0;
                    size_t Xa;
                };
                using ActionNodes = std::vector<ActionNode>;

                struct BeliefNode {
                    BeliefNode() : N(0) {}
                    BeliefNode(size_t s) : belief(1, s), N(0) {}
                    BeliefNode(size_t s, double r, int o) : belief(1, s), N(0),
                                                            rem(r), obs(o) {}
                    ActionNodes children;
                    std::vector<int> support;
                    SampleBelief belief;
                    std::map<size_t,double> sparseBel; // associates to a state number, the probability that it is the actual current state
                    unsigned N;
                    double rem = 0.0;
                    bool end = false;// is this node terminal
                    int obs = -1;
                    double proba = -1;// probability to be reached after the previous action
                    double reward=0;// reward obtained after getting the corresponding observation
                    bool explored=false;// was this node already explored with exact updates
                    double confidence = 0;
                };

                bool timeout_occured = false;
                double confidence_;
                unsigned k_ = 0;

                /**
                 * @brief Basic constructor.
                 *
                 * @param m The POMDP model that BWCPOMCP will operate upon.
                 * @param beliefSize The size of the initial particle belief.
                 * @param iterations The number of episodes to run before completion.
                 * @param exp The exploration constant. This parameter is VERY
                 * important to determine the final BWCPOMCP performance.
                 * @param t The threshold for the worst-case value
                 */
                BWCPOMCP(const M& m, size_t beliefSize, unsigned iterations, double conf,
                         double t, double exp, size_t sm, size_t lp, BWC::POMDP* p, const std::string timeFileName);

                /**
                 * @brief This function resets the internal graph and samples for the provided belief and horizon.
                 *
                 * In general it would be better if the belief did not contain
                 * any terminal states; although not necessary, it would
                 * prevent unnecessary work from being performed.
                 *
                 * @param b The initial belief for the environment.
                 * @param horizon The horizon to plan for.
                 *
                 * @return The best action.
                 */
                size_t sampleAction(const Belief& b, unsigned horizon);

                /**
                 * @brief This function uses the internal graph to plan.
                 *
                 * This function can be called after a previous call to
                 * sampleAction with a Belief. Otherwise, it will invoke it
                 * anyway with a random belief.
                 *
                 * If a graph is already present though, this function will
                 * select the branch defined by the input action and
                 * observation, and prune the rest. The search will be started
                 * using the existing graph: this should make search faster,
                 * and also not require any belief updates.
                 *
                 * NOTE: Currently there is no particle reinvigoration
                 * implemented, so for long horizons you can expect
                 * progressively degrading performances.
                 *
                 * @param a The action taken in the last timestep.
                 * @param o The observation received in the last timestep.
                 * @param horizon The horizon to plan for.
                 *
                 * @return The best action.
                 */
                size_t sampleAction(size_t a, size_t o, unsigned horizon);

                /**
                 * @brief This function sets the new size for initial beliefs created from sampleAction().
                 *
                 * Note that this parameter does not bound particle beliefs
                 * created within the tree by result of rollouts: only the ones
                 * directly created from true Beliefs.
                 *
                 * @param beliefSize The new particle belief size.
                 */
                void setBeliefSize(size_t beliefSize);

                /**
                 * @brief This function sets the number of performed rollouts in BWCPOMCP.
                 *
                 * @param iter The new number of rollouts.
                 */
                void setIterations(unsigned iter);

                /**
                 * @brief This function sets the new aimed confidence
                 *
                 */
                void setConfidence(double conf);

                /**
                 * @brief This function returns the POMDP generative model being used.
                 *
                 * @return The POMDP generative model.
                 */
                const M& getModel() const;

                /**
                 * @brief This function returns a reference to the internal
                 * graph structure holding the results of rollouts.
                 *
                 * @return The internal graph.
                 */
                const BeliefNode& getGraph() const;

                /**
                 * @brief This function returns the initial particle size for converted Beliefs.
                 *
                 * @return The initial particle count.
                 */
                size_t getBeliefSize() const;

                /**
                 * @brief This function returns the number of iterations performed to plan for an action.
                 *
                 * @return The number of iterations.
                 */
                unsigned getIterations() const;

                /**
                 * @brief This function returns the currently set exploration constant.
                 *
                 * @return The exploration constant.
                 */
                double getConfidence() const;

                double getNewConfidence(size_t action, size_t obs) const;
                double valL(const BeliefNode & b) const;
                                void testcopygraph();


            private:
                const M& model_;
                size_t S, A, O, beliefSize_, simulationMethod_, linearProgramming_;
                unsigned iterations_, maxDepth_;
                double threshold_;
                double penalty_;
                double exploration_;
                bool first_;
                BWC::POMDP* pomdp_;
                const std::string timeFileName_;
                boost::timer::nanosecond_type initTimeout_;
                boost::timer::nanosecond_type runTimeout_;
                
                std::mt19937_64 rng_;
  							std::uniform_real_distribution<double> distribution_;

                SampleBelief sampleBelief_;
                BeliefNode graph_;

                mutable std::default_random_engine rand_;

                /**
                 * @brief This function starts the simulation process.
                 *
                 * This function simply calls simulate() for the number of
                 * times specified by BWCPOMCP's parameters. While doing so it
                 * builds a tree of explored outcomes, from which BWCPOMCP will
                 * then extract the best expected action for the current
                 * belief.
                 *
                 * @param horizon The horizon for which to plan.
                 *
                 * @return The best action to take given the final built tree.
                 */
                size_t runSimulation(unsigned horizon);

                 /**
                 * @brief This function launches a simulation like for POMDP.
                 *
                 * @param q is the list of the <action, observation, rewards> met during the simulation.
                 */
								std::tuple<double,bool> simulatePOMCP(BeliefNode & b, size_t s, 
																	unsigned horizon, std::queue<std::tuple<size_t,size_t,double>> &q);            
								
								/**
                 * @brief This function re-do a successful simulation by following "q", it computes the belief states met 
                 * during the simulation.
                 *
                 */     
								double simulateExact(BeliefNode & b, unsigned horizon, std::queue<std::tuple<size_t,size_t,double>> &q);

								/**
                 * @brief The 3 following functions computes respectively the U L and V values of a belief Node.
                 *
                 */ 
								double valU(const BeliefNode & b);
								double valV(const BeliefNode & b);
								
								/**
                 * @brief Computes the probability to get the observation "observation" when playing the action "a" from 
                 * the belief state "b"
                 *
                 */
								double exactProba(size_t a, BeliefNode & b, size_t observation);

								/**
                 * @brief [Old] Used to truncate the probabilities in a belief state. 
                 *
                 */ 
								void truncateBelief(size_t magnitude, BeliefNode & b);

                /**
                 * @brief This function finds the best action w.r.t. the V value.
                 */
                size_t findBestA(BeliefNode &b);
                
                /**
                 * @brief This function finds the best action w.r.t. the L and V values.
                 */
                size_t findBestAE(BeliefNode &b);                              

                /**
                 * @brief This function finds the best action w.r.t. the POMCP criterion.
                 */
                size_t findBestBonusA(BeliefNode &b);
                
                /**
                 * @brief This function samples a given belief in order to produce a particle approximation of it.
                 *
                 * @param b The belief to be approximated.
                 *
                 * @return A particle belief approximating the input belief.
                 */
                SampleBelief makeSampledBelief(const Belief & b);

								/**
                 * @brief This function turns the original belief state into a map.
                 */
                std::map<size_t,double> makeSparseBelief(const Belief & b);

								/**
                 * @brief This function computes the belief state of a child given an action and a belief state and observation.
                 */
								void updateBelief(const BeliefNode &b, size_t a, BeliefNode &c, size_t obs);
								
                /**
                 * @brief Classical rollout that remembers the actions played and the observations received.
                 */
                double rollout(const BeliefNode &b, size_t s, unsigned horizon, std::queue<std::tuple<size_t,size_t,double>> &q);
                
                /**
                 * @brief This function samples a state from a belief node.
                 */
                size_t sampleState(const BeliefNode &b);
                
                /**
                 * @brief This function checks if a list [<c,v>...] is valid.
                 */
                bool checkList(std::map<double,double> & l);
                
                /**
                 * @brief The 4 following functions builds the list [<c,v>...]. The function is different for the belief 
                 * and the action nodes. The "final" functions are for the root of the tree and its children as the different 
                 * confidences have to be remembered.
                 */
                std::map<double,double> actionGather(ActionNode & aNode);
                std::map<double,double> beliefGather(BeliefNode & b);
                /**
                 * @return <c,<v,[confidence_for_obs_1,confidence_for_obs_2,...]>>
                 *         confidence -> value + how to distribute confidences in the belief successors of the action
                 */
                std::map<double,std::pair<double,std::vector<double>>> finalActionGather(ActionNode & aNode);
                /**
                 * @return <c,<v,<corresponding_action,[confidence_for_obs_1,confidence_for_obs_2,...]>>>
                 * 				 confidence -> value + (action to play,how to distribute confidences in the belief successors of the action)
                 */
                std::map<double,std::pair<double,std::pair<size_t,std::vector<double>>>> finalBeliefGather(BeliefNode & b);
                
                /**
                 * @brief Finds the next action through linear optimization
                 */
                
                std::pair<size_t,std::vector<double>> linearSolving(BeliefNode & b);
                                
                /**
                 * @brief Searches the exploration tree to build the constraints and the objective
                 *        function (is recursive), used by linearSolving
                 * @param aNode current action node
                 * @param env environment for CPLEX
                 * @param model model for CPLEX
                 * @param vars array of variables, each corresponds to an action node
                 * @param curr integer used to keep track of what is the position of the variable 
                 * in vars corresponding to the next action node 
                 * @param confConstr is the expression corresponding to the confidence constraint
                 * @param reward is the objective function
                 * @param discount is the discounting factor at this point in the tree
                 * 
                 */
                
                void linearSolvingExprConstruction(ActionNode & aNode, IloEnv& env, IloModel& model, IloNumVarArray& vars, size_t& curr, IloExpr& confConstr, IloExpr& reward, double discount);
                
               /**
                * @brief Computes the confidence associated to the action wrt the current assignation of the variables
                *
                */
								double linearGetConfidences(ActionNode & aNode, IloNumVarArray& vars, IloCplex& cplex);
                
                ActionNode copyAN(const ActionNode& aNode) const;
                
                BeliefNode copyBN(const BeliefNode& b) const;
                
                BeliefNode copyGraph() const;
                
        };
        
				
        template <typename M>
        BWCPOMCP<M>::BWCPOMCP(const M& m, size_t beliefSize,
                              unsigned iter, double conf, double t, double exp, size_t sm,
                              size_t lp, BWC::POMDP* p, const std::string timeFileName) : 
                              								 model_(m),
                              					timeFileName_(timeFileName),
                                               S(model_.getS()),
                                               A(model_.getA()),
                                               O(model_.getA()),
                                               beliefSize_(beliefSize),
                                               iterations_(iter),
                                               confidence_(conf),
                                               graph_(),
                                               rand_(Impl::Seeder::getSeed()),
                                               threshold_(t),
                                               pomdp_(p),
                                               exploration_(exp),
                                               simulationMethod_(sm),
                                               linearProgramming_(lp) {
                                               first_ = true;
                                               initTimeout_ = 5 * 1000000000LL;
                                               runTimeout_ = 0.1 * 1000000000LL;
                                               /*unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    																					 std::uniform_real_distribution<double> distribution_(0.0, 1.0);
    																					 std::mt19937_64 rng_(seed);*/
                                               }
                                               
        template <typename M>
				typename BWCPOMCP<M>::ActionNode BWCPOMCP<M>::copyAN(const ActionNode& aNode) const {
		      ActionNode copy;

		      copy.V = aNode.V;
		      copy.U = aNode.U;
		      copy.L = aNode.L;
		      copy.N = aNode.N;
		      copy.oN = aNode.oN;

		      for(auto& belo:aNode.children){
		      	copy.children[belo.first]=copyBN(belo.second);
		      }

		      return copy;
				}

 				template <typename M>
        typename BWCPOMCP<M>::BeliefNode BWCPOMCP<M>::copyBN(const BeliefNode& b) const{
				  BeliefNode copy;
				  copy.N = b.N;
				  copy.rem = b.rem;
				  copy.end = b.end;
				  copy.obs = b.obs;
				  copy.proba = b.proba;
				  copy.reward = b.reward;
				  copy.explored = b.explored;
				  copy.confidence = b.confidence;

				  for(auto& aNode:b.children) {
				  	copy.children.push_back(copyAN(aNode));
				  }
				  for(auto x:b.support){
				  	copy.support.push_back(x);
				  }
				  for(auto sp:b.sparseBel){
				  	copy.sparseBel[sp.first] = sp.second;
				  }

				  return copy;
				}
				
				
				template <typename M>
        void BWCPOMCP<M>::testcopygraph(){
        	BeliefNode b1;
        	b1.rem = 30;
        	ActionNode a1;
        	a1.L=10;
        	b1.children.push_back(a1);
        	BeliefNode b2 = copyBN(b1);
        	b2.rem = 50;
        	b2.children[0].L=0;
        	std::cout << "*****************************\n";
        	std::cout << "b1rem " << b1.rem << "    a1L " << a1.L << "     b2rem " << b2.rem << "      a2L "<< b2.children[0].L << "    b1c " << b1.children[0].L <<"\n";
        }
				
				template <typename M>
        typename BWCPOMCP<M>::BeliefNode BWCPOMCP<M>::copyGraph() const{
        	return copyBN(graph_);
        }

        template <typename M>
        size_t BWCPOMCP<M>::sampleAction(const Belief& b, unsigned horizon) {
            // Reset graph
            unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
            rng_.seed(seed);
           
            graph_ = BeliefNode(A);
            graph_.children.resize(A);
            graph_.belief = makeSampledBelief(b);
            graph_.sparseBel = makeSparseBelief(b);
            if(TRUNC) truncateBelief(MAGNITUDE, graph_); 
            graph_.confidence = confidence_;
            graph_.rem = threshold_;
            graph_.support = pomdp_->getStatesInBelief(b, -1);
            
            
            return runSimulation(horizon);
        }

        template <typename M>
        size_t BWCPOMCP<M>::sampleAction(size_t a, size_t o, unsigned horizon) {
            auto & obs = graph_.children[a].children;
            auto it = obs.find(o);
            if ( it == obs.end() ) {
            		//if the observation node was never built
            		double rew = pomdp_->getRewardObs(o);
                graph_.children[a].children.emplace(std::piecewise_construct,
                                       std::forward_as_tuple(o),
                                       std::forward_as_tuple(0, graph_.rem -rew, o)); 
                it = graph_.children[a].children.find(o);
                it->second.support = pomdp_->postInObs(graph_.support, a, o);
                it->second.reward = rew;
                updateBelief(graph_, a, it->second, o);
            }

            // Here we need an additional step, because *it is contained by graph_.
            // If we just move assign, graph_ is first going to delete everything it
            // contains (included *it), and then we are going to move unallocated memory
            // into graph_! So we move *it outside of the graph_ hierarchy, so that
            // we can then assign safely.
            { auto tmp = std::move(it->second); graph_ = std::move(tmp); }
            if ( ! graph_.belief.size() ) {
                std::cerr << "BWCPOMCP Lost track of the belief, restarting with uniform..\n";
                auto b = Belief(S); b.fill(1.0/S);
                return sampleAction(b, horizon);
            }

            // We resize here in case we didn't have time to sample the new
            // head node. In this case, the new head may not have children.
            // This would break the UCT call.
            graph_.children.resize(A);
            
            for(auto & po: graph_.sparseBel) {
            	std::cout << po.first <<" " << po.second<< "\n";
            }
            
            confidence_ = graph_.confidence;
            std::cout << "current confidence: " << confidence_ << std::endl;
            
            return runSimulation(horizon);
        }

        //returns action to play after doing simulation
        template <typename M>
        size_t BWCPOMCP<M>::runSimulation(unsigned horizon) {
          if ( !horizon ) return 0;
					boost::timer::cpu_timer t;
          maxDepth_ = horizon;
          std::uniform_int_distribution<size_t> generator(0, graph_.belief.size()-1);
          unsigned k=0;
          bool runTimeoutOccured = false;
        	//for (unsigned i = 0; i < iterations_; ++i ) {
          	while (!first_ && ((k < iterations_ || valL(graph_) < confidence_) && (!runTimeoutOccured) )) {
          		k = k<iterations_ ? k+1 : k;
                //std::cout << k;
          		runTimeoutOccured = ((t.elapsed()).wall >= runTimeout_);
        		std::queue<std::tuple<size_t,size_t,double>> q;
						// here we perform a trial POMCP simulation to find paths that go over the threshold
						// if the POMCP simulation succeeds, we recompute exact belief updates along the path to determine L, U, etc... values
						double rew; bool explored;
						if(simulationMethod_ == 0) {std::tie(rew, explored) = simulatePOMCP(graph_, sampleState(graph_), 0,q);}
						else if (simulationMethod_ == 1) {rew = rollout(graph_, sampleState(graph_), 0, q); explored = false;}
        	  if ((rew>=threshold_ && !explored) || ALL_EXACT_UPDATES) {
        			simulateExact(graph_, 0, q);
        		}
      		}
					
					if(LOOP_WHILE && first_){
                        //unsigned k=0;
						while((k<iterations_ ||valL(graph_)<confidence_) && !timeout_occured) {
                            k = k < iterations_ ? k+1 : k;
                            //std::cout << k << ";" << iterations_ << "\n";
							std::queue<std::tuple<size_t,size_t,double>> q;
							double rew; bool explored;
							if(simulationMethod_ == 0) {std::tie(rew, explored) = simulatePOMCP(graph_, sampleState(graph_), 0,q);}
							else if (simulationMethod_ == 1) {rew = rollout(graph_, sampleState(graph_), 0, q); explored = false;}
		      	  if ((rew>=threshold_ && !explored) || ALL_EXACT_UPDATES) {
		      			simulateExact(graph_, 0, q);
        			}
        				timeout_occured = ((t.elapsed()).wall <= initTimeout_ ) ? false : true;
						}
					}
					
					for(int i = 0; i< graph_.children.size();++i) {
						for(auto &b : graph_.children[i].children) {
							std::cout << "  action " << i 
												<< "  obs " << pomdp_->getObsName(b.first) 
												<< "  proba " << exactProba(i,graph_,b.first) <<"\n";
							std::cout << "L " <<valL(b.second) << "  U " << valU(b.second) <<"\n";
						}
						std::cout<< "V " << graph_.children[i].V <<"\n";
					}
					
					std::cout <<"\n";
					std::cout <<"Exploration done\n";
					std::cout <<"\n";
					
        	int bestA;
					
					std::cout << "valL(graph_) "<< valL(graph_) <<"   confidence_" << confidence_ << "\n";
					
					//gathering and establishing new confidences
        	if (!graph_.end && confidence_ > 0 && valL(graph_)>=confidence_) {
        		if(!linearProgramming_){
        			std::map<double,std::pair<double,std::pair<size_t,std::vector<double>>>> gathering = finalBeliefGather(graph_);
		        	auto it = gathering.lower_bound(confidence_);
		      		bool max_confidence = (it == gathering.end());
				    	if (max_confidence) {--it;} 
				    	bestA = it->second.second.first;
				    	
				    	std::cout << "list:  ";
							for(auto & r:gathering) {std::cout << "<" << r.first << "," << r.second.first << ">";}
				    	std::cout << "\n";
				    	
				    	int i=0;
							for(auto & c:graph_.children[bestA].children) {
								updateBelief(graph_, bestA, c.second, c.first);
								c.second.confidence = it->second.second.second[i];
								++i;
							}
		      	} else {
		      		std::vector<double> confidences;
		      		std::tie(bestA,confidences) = linearSolving(graph_);
		      		
		      		if(confidence_==0){
		      			for(auto & c:graph_.children[bestA].children) {
									updateBelief(graph_, bestA, c.second, c.first);
									c.second.confidence = 0;
								}
		      		} else {
		      			int i=0;
		      			for(auto & c:graph_.children[bestA].children) {
									updateBelief(graph_, bestA, c.second, c.first);
									c.second.confidence = confidences[i];
									++i;
								}
		      		}
		      		
		      	}
        	} else {// if the simulation is over/confidence is broken or zero
        		//bestA = findBestAE(graph_);
        		if(confidence_ <= 0) {
        			bestA = findBestA(graph_);
        			for(auto & c:graph_.children[bestA].children) {
								updateBelief(graph_, bestA, c.second, c.first);
								c.second.confidence = 0;
					}
        		}	else {
        			double vallgraph = valL(graph_);
        			bestA = findBestAE(graph_);
		      		if(vallgraph>0) {
				    	for(auto & c:graph_.children[bestA].children) {
							updateBelief(graph_, bestA, c.second, c.first);
							c.second.confidence = (vallgraph<=confidence_? 1 :confidence_ / vallgraph);
						}
				  	//} //else if (confidence_==0) {
				  		//for(auto & c:graph_.children[bestA].children) {
						//		updateBelief(graph_, bestA, c.second, c.first);
					//			c.second.confidence = 0;
						//	}
				  	} else {
				  		for(auto & c:graph_.children[bestA].children) {
							updateBelief(graph_, bestA, c.second, c.first);
							c.second.confidence = 1;
						}
				  	}
        		}
        	
        	}
        	//resize the children and define next confidence
        	
					first_ = false;
					
					// std::cout << t.elapsed() << std::endl;
					std::ofstream outFile (timeFileName_, std::ios::app);
					if (outFile.is_open()) {
							outFile << t.format(4,"%u") << ", ";
							outFile.close();
						}
					else std::cout << "Unable to open outFile";

                    k_ = k;
					
        	return bestA;
        }
        
        template <typename M>
        double BWCPOMCP<M>::valU(const BeliefNode & b) {
        	if(b.end && b.rem>0.0) {return 0.0;}
        	if(b.end && b.rem<=0.0) {return 1.0;}
        	if(b.children.size()<A) {return 1.0;}
        	double val = 0.0;
        	for (auto &a : b.children) {
        		if (a.U>val) {val = a.U;};
        	}
        	return val;
        }
        
        template <typename M>
        double BWCPOMCP<M>::valL(const BeliefNode & b) const {
        	if(b.end && b.rem>0.0) {return 0.0;}
        	if(b.end && b.rem<=0.0) {return 1.0;}
        	double val = 0.0;
        	for (auto &a : b.children) {
        		if (a.L>val) {val = a.L;}
        	}
        	return val;
        }
        
        template <typename M>
        size_t BWCPOMCP<M>::findBestA(BeliefNode &b) {
            // std::cout << "instrumented findBestA called" << std::endl;
            std::vector<int> indices(A);
            std::iota(indices.begin(), indices.end(), 0);

            std::sort(indices.begin(),
                      indices.end(),
                      [&b](int lhs, int rhs) {
                          return (b.children[lhs].V < b.children[rhs].V);
                      });
            /* std::cout << "The best actions, in increasing order: ";
             * for (auto it = indices.begin(); it != indices.end(); ++it)
             *     std::cout << *it << " ";
             * std::cout << std::endl;
             */
						auto & aNode = b.children[indices.back()];
						//std::cout << "V: "<< aNode.V << "   L: "<< aNode.L << "   U: "<< aNode.U << "\n" << "bestA: " << indices.back() <<"\n";
            return indices.back();
        }

        template <typename M>
        size_t BWCPOMCP<M>::findBestBonusA(BeliefNode &b) {
        		unsigned count = b.N;
            // Count here can be as low as 1.
            // Since log(1) = 0, and 0/0 = error, we add 1.0.
            double logCount = std::log(count + 1.0);
            // We use this function to produce a score for each action. This can be easily
            // substituted with something else to produce different BWCPOMCP variants.
            auto evaluationFunction = [this, logCount](const ActionNode & an){
                    return an.V + exploration_ * std::sqrt( logCount / an.N );
            };

            std::vector<int> indices(A);
            std::iota(indices.begin(), indices.end(), 0);

            std::sort(indices.begin(),
                      indices.end(),
                      [&b, &evaluationFunction](int lhs, int rhs) {
                          return evaluationFunction(b.children[lhs]) <
                               evaluationFunction(b.children[rhs]);
                      });
            return indices.back();
        }
       
        template <typename M>
        typename BWCPOMCP<M>::SampleBelief BWCPOMCP<M>::makeSampledBelief(const Belief & b) {
            SampleBelief belief;
            belief.reserve(beliefSize_);

            for ( size_t i = 0; i < beliefSize_; ++i )
                belief.push_back(sampleProbability(S, b, rand_));

            return belief;
        }
        
        template <typename M>
        std::map<size_t,double> BWCPOMCP<M>::makeSparseBelief(const Belief & b) {
        	std::map<size_t,double> belief;
        	for(int i=0; i< b.size(); ++i) {
        		if(b[i]>0) {belief[i]=b[i];}
        	}
        	return belief;
        }

        template <typename M>
        void BWCPOMCP<M>::setBeliefSize(size_t beliefSize) {
            beliefSize_ = beliefSize;
        }

        template <typename M>
        void BWCPOMCP<M>::setIterations(unsigned iter) {
            iterations_ = iter;
        }

        template <typename M>
        void BWCPOMCP<M>::setConfidence(double conf) {
            confidence_ = conf;
        }

        template <typename M>
        const M& BWCPOMCP<M>::getModel() const {
            return model_;
        }

        template <typename M>
        const typename BWCPOMCP<M>::BeliefNode& BWCPOMCP<M>::getGraph() const {
            return graph_;
        }

        template <typename M>
        size_t BWCPOMCP<M>::getBeliefSize() const {
            return beliefSize_;
        }

        template <typename M>
        unsigned BWCPOMCP<M>::getIterations() const {
            return iterations_;
        }

        template <typename M>
        double BWCPOMCP<M>::getConfidence() const {
            return confidence_;
        }
        
        
        template <typename M>
        double BWCPOMCP<M>::simulateExact(BeliefNode & b, unsigned depth, std::queue<std::tuple<size_t,size_t,double>> &q) {
            //std::cout << "Sim exact\n";
            b.N++;
            size_t a,o; double rew;
            std::tie(a,o,rew) = q.front(); q.pop();
            
		/// why is rem not keeped as a part of a node?
            double rem = (b.rem - rew) / model_.getDiscount();

            auto & aNode = b.children[a];

            {
                double futureRew = 0.0;
                auto ot = aNode.children.find(o);
                if ( ot == std::end(aNode.children) ) {
                    aNode.children.emplace(std::piecewise_construct,
                                           std::forward_as_tuple(o),
                                           std::forward_as_tuple(0, rem, o));
                    ot = aNode.children.find(o);
                    ot->second.support = pomdp_->postInObs(b.support, a, o);
                    ot->second.reward = rew;
                    updateBelief(b, a, ot->second, o);
                    if ( !q.empty() ) {
                    	ot->second.children.resize(A);
                    	futureRew = simulateExact(ot->second, depth + 1, q);
										} else {
											ot->second.end=true;
											futureRew = 0;
										}
										ot->second.explored = true;
										
                }
                else {
                	updateBelief(b, a, ot->second, o);
                  if ( !q.empty() ) {
                      ot->second.children.resize(A);
                      futureRew = simulateExact( ot->second, depth + 1, q);
              		} else {ot->second.end=true;}
              		ot->second.explored = true;
                	rew += model_.getDiscount() * futureRew;
                }
            }
						// Exact belief and other value update is done below.
						/// There might be an error, below. It feels to me that we are updating N and V twice. Maybe not error, but unnecessary?
						aNode.N++;
						aNode.V += ( rew - aNode.V ) / static_cast<double>(aNode.N);
						aNode.L = 0.0;
						double accum = 0.0;
						for (auto & bel : aNode.children) {
							if(bel.second.proba==-1) bel.second.proba = exactProba(a,b,bel.first);
							double proba = bel.second.proba;
							double probaVall = proba*valL(bel.second);
							aNode.L += probaVall;
                            //assert(aNode.L <= 1.0);
							accum += proba; 
						}
                    assert(accum<=1.0 + CEPS);
          	return rew;
      	}
      	
      	template <typename M>
      	void BWCPOMCP<M>::updateBelief(const BeliefNode &b, size_t a, BeliefNode &c, size_t obs) {
		    	if(c.sparseBel.empty()) {
		        double weight = 0.0;
                double testbel2 = 0.0;
                for(auto& po2:b.sparseBel) {testbel2 += po2.second;}
                assert(testbel2 <=1.0 + CEPS);
                for(auto const target: c.support) {c.sparseBel[target] = 0.0; }
		        for(auto const stateProb: b.sparseBel) {
		        auto const pd = pomdp_->getProbDelta();
                auto const obp = pomdp_->getProbObs();
		        	for(auto const target: c.support) {
		        		auto it = pd.find(std::tuple<int, int, int>(stateProb.first,a,target));
                        auto it2 = obp.find(std::tuple<int, int>(target,obs));
		        		if(it != pd.end() && it2 != obp.end() ){
		        			 c.sparseBel[target]+=it->second*stateProb.second*it2->second;
		        			 weight += it->second*stateProb.second*it2->second;
		        		}
		        	}
		        }
		        
		        for(auto& po:c.sparseBel) {po.second/=weight;}
                double testbel = 0.0;
                for(auto& po:c.sparseBel) {testbel += po.second;}
                assert(testbel <=1.0 + CEPS);
		        
		        if(TRUNC) truncateBelief(MAGNITUDE, c);
		    	}
      	}

    		template <typename M>
        double BWCPOMCP<M>::exactProba(size_t a, BeliefNode & b, size_t observation) {
      		double proba = 0.0;
      		auto const pd = pomdp_->getProbDelta();
            auto const obs = pomdp_->getProbObs();
      		for(const auto stateProb: b.sparseBel) {
      			for(const auto target: b.children[a].children[observation].support) {
          		auto it = pd.find(std::tuple<int, int, int>(stateProb.first,a,target));
                auto it2 = obs.find(std::tuple<int,int>(target,observation));
              if(it != pd.end() && it2 != obs.end() && it->second>0.0 && it2->second>0.0){
              	proba += it->second*stateProb.second*it2->second;
              }
          	}
          }
            assert(proba <=1.0 + CEPS);
        	return proba;
        }
        
        template <typename M>
        void BWCPOMCP<M>::truncateBelief(size_t magnitude, BeliefNode & b) {
        	double po = pow(10,magnitude);
        	double total =0;
        	for(auto & stateProb:b.sparseBel) {
        		if(stateProb.second<po){
        			stateProb.second = floor(stateProb.second*po)/po;
        		} else {total += stateProb.second;}
        	}
        	for(auto & stateProb:b.sparseBel) {
        		if(stateProb.second!=0){
        			stateProb.second = (stateProb.second)/total;
        		}
        	}
        }
        
        template <typename M>
        double BWCPOMCP<M>::valV(const BeliefNode & b) {
        	if (b.children.empty()) {return 0;}
        	double V = b.children[0].V;
        	for (auto &a : b.children) {
        		if(a.V>V) {V=a.V;}
        	}
        	return V;
        }
        
        template <typename M>
        size_t BWCPOMCP<M>::findBestAE(BeliefNode &b) {
            std::vector<int> indices(A);
            std::iota(indices.begin(), indices.end(), 0);

            std::sort(indices.begin(),
                      indices.end(),
                      [&b,this](int lhs, int rhs) {
                          return ((!(b.children[lhs].L >= confidence_ && b.children[rhs].L < confidence_)) &&
				                    ( (b.children[rhs].L >= confidence_ && b.children[lhs].L < confidence_) ||
				                      (
			                      		((b.children[lhs].L < confidence_)
			                      					&&((b.children[lhs].L < b.children[rhs].L) && (b.children[lhs].L != b.children[rhs].L))) ||
			                      		((b.children[lhs].V < b.children[rhs].V) 
			                      					&&(b.children[lhs].L >= confidence_ || b.children[lhs].L == b.children[rhs].L))
				                      )
				                    ));
                      });
            
            /*std::cout << "Action selection:\n";
            for(int i=0;i<A;++i){
            	auto & aNode = b.children[indices[i]];
            	std::cout << "V: "<< aNode.V << "   L: "<< aNode.L << "   U: "<< aNode.U << "\n" << "action: " << indices[i] <<"\n";
            }*/
            
						auto & aNode = b.children[indices.back()];
						std::cout << "V: "<< aNode.V << "   L: "<< aNode.L << "   U: "<< aNode.U << "\n" << "bestA: " << indices.back() <<"\n";
            return indices.back();
        }
        
        template <typename M>
        double BWCPOMCP<M>::getNewConfidence(size_t action, size_t obs) const {
        	double newConf = confidence_; 
        	double proba=0.0000000001L;
        	for(auto &b : graph_.children[action].children) {
        		if(obs != b.first) {
                    assert(b.second.proba!=-1);
        			newConf -= b.second.proba*valL(b.second);
        		} else {proba = b.second.proba;}
        	}
        	return newConf/proba;
        }
        
        template <typename M>
        std::tuple<double,bool> BWCPOMCP<M>::simulatePOMCP(BeliefNode & b, size_t s, unsigned depth, std::queue<std::tuple<size_t,size_t,double>> &q) {
            b.N++;

            size_t a = findBestBonusA(b);
            bool explored;
            
            size_t s1, o; double rew;
            std::tie(s1, o, rew) = model_.sampleSOR(s, a);
            // std::cout << "SIM: played " << a << " and ontained observation " << o
            //           << " with reward " << rew << std::endl;
            double rem = (b.rem - rew) / model_.getDiscount();
            // std::cout << "SIM: new remainder = " << rem << std::endl;

            auto & aNode = b.children[a];
			q.push(std::tuple<size_t,size_t,double>(a,o,rew));
            {
                double futureRew = 0.0;
                // We need to append the node anyway to perform the belief
                // update for the next timestep.
                auto ot = aNode.children.find(o);
                if ( ot == std::end(aNode.children) ) {
                    aNode.children.emplace(std::piecewise_construct,
                                           std::forward_as_tuple(o),
                                           std::forward_as_tuple(s1, rem, o));
                    ot = aNode.children.find(o);
                    ot->second.support = pomdp_->postInObs(b.support, a, o);
                    ot->second.reward = rew;
                    // This stops automatically if we go out of depth
                    futureRew = rollout(ot->second, s1, depth + 1, q);
                    explored = false;
                }
                else {
                    //ot->second.belief.push_back(s1);
                    // We only go deeper if needed (maxDepth_ is always at least 1).
                    if ( depth + 1 < maxDepth_ && !model_.isTerminal(s1) ) {
                        // Since most memory is allocated on the leaves,
                        // we do not allocate on node creation but only when
                        // we are actually descending into a node. If the node
                        // already has memory this should not do anything in
                        // any case.
                        ot->second.children.resize(A);
                        std::tie(futureRew,explored) = simulatePOMCP( ot->second, s1, depth + 1, q);
                    } else {
                    	ot->second.end=true;
                    	explored = ot->second.explored;
                    }
                    // here, one would make ot->second update its rem to take
                    // the min between what it has and the local variable rem,
                    // however, for observable weights this is not necessary
                }

                rew += model_.getDiscount() * futureRew;
            }

            // Action update
            aNode.N++;
            aNode.V += ( rew - aNode.V ) / static_cast<double>(aNode.N);

            return std::tuple<double,bool> (rew,explored);
        }
        
	// here the queue stores AOR tuples
        template <typename M>
        double BWCPOMCP<M>::rollout(const BeliefNode &b, size_t s, unsigned depth, std::queue<std::tuple<size_t,size_t,double>> &q) {
            double rew = 0.0, totalRew = 0.0, gamma = 1.0;
            if (model_.isTerminal(s)) {return totalRew;}
						size_t o,a;
            // rolling out unsafely from BWC
            std::uniform_int_distribution<size_t> generator(0, A-1);
            for ( ; depth < maxDepth_; ++depth ) {
                a = generator(rand_);
                std::tie( s, o, rew ) = model_.sampleSOR( s, a );
                q.push(std::tuple<size_t,size_t,double>(a,o,rew));
                								
                totalRew += gamma * rew;
                gamma *= model_.getDiscount();
                if (model_.isTerminal(s)) {break;}
            }
            return totalRew;
        }
        
        template <typename M>
        size_t BWCPOMCP<M>::sampleState(const BeliefNode &b) {
        	/*double count =0;
        	for(auto &c:b.sparseBel) {count +=c.second;}
        	
        	if(count<0.999) {
        		std::cout << "invalid belief state:" << count << "\n";
        		assert(false);
        	}*/
        	
					double ran = distribution_(rng_);
   				for(auto& stateProb: b.sparseBel) {
   					if(ran<stateProb.second) {return stateProb.first;}
   					else { ran -=  stateProb.second;}
   				}
        }
        
        template <typename M>
        std::map<double,double> BWCPOMCP<M>::actionGather(ActionNode & aNode) {
        	//std::cout << "call act\n";
        	//std::map<double,double> backup;
        	std::map<double,double> result;
        	// a storage for the results of the children
        	// there is an ugly name confusion here
        	std::vector<std::pair<double,std::map<double,double>>> children;
        	// a storage for iterators on the previous maps, i.e. initially points to the beginning of the map
        	std::vector<std::map<double,double>::iterator> childrenIter;
        	
        	if(aNode.L==0){
        		result[0]=aNode.V;
        		return result;
        	}
        	
        	for(auto & c:aNode.children) {
        		children.push_back(std::pair<double,std::map<double,double>>(c.second.proba,beliefGather(c.second)));
        		childrenIter.push_back(children.back().second.begin());
        	}
        	
        	while(true) {// until every combination has been explored
        		//values of the current pair
        		double c = 0;
        		double v = 0;
        		for(int i=0; i<children.size();++i) {
        			c += children[i].first* childrenIter[i]->first;
        			v += children[i].first* childrenIter[i]->second;  
        		}
        		
        		//inserting in the resulting list
        		auto it = result.find(c);
  					if (it != result.end()) {
  						if (it->second<v) {
  							result[c] = v;
  							//backup[c] = v;
  						}
  					} else {
  						result[c] = v;
  						//backup[c] = v;
  					}
  					
  					// deleting the possible useless neigbour
						auto it2 = result.upper_bound(c);
								
						if (it2 != result.end() && it2->second >= v) {
				  		result.erase(c);
				  	}
				  	
				  	it = result.find(c);

						if (it != result.begin() && it != result.end()) {
							while(it != result.begin()) {
								it2=std::prev(it,1);
								if (it2->second <= v) {
									result.erase(it2);
								} else {break;}
							}
			  		}
	      		
	      		//finding the next combination
	      		int j=0;
	      		for(j;j<childrenIter.size();++j) {
	      			++childrenIter[j];
	      			if (childrenIter[j]==children[j].second.end()) {
	      				childrenIter[j]==children[j].second.begin();
	      			} else {break;}
	      		}

	      		if (j == childrenIter.size()) {break;}
        	}
        	
        	assert(checkList(result));
        	return result;
        }
        
        
        template <typename M>
        std::map<double,double> BWCPOMCP<M>::beliefGather(BeliefNode & b) {
		//gather list for a non-root node
		//i.e. confidence-value list
        	//std::cout << "call bel\n";
        	//std::map<double,double> backup;
        	//could be optimized without doing all that copying?
        	std::map<double,double> result;

        	if (valL(b) == 0) {//termination
        		result[0] = valV(b);
        	} else if (b.end) {//termination
        		result[valL(b)] = valV(b);
        	} else {
        		size_t i=0;//number of the action
        		for (auto & a: b.children) {//for all children merge the lists
        			if(a.L!=0){
        				for(auto & c:a.children) {
						  		if(c.second.proba==-1) c.second.proba = exactProba(i,b,c.first);
						  	}
        			}
        			++i;
        			std::map<double,double> tmp = actionGather(a);
        			double c,v;
        			for(auto &cv:tmp) {
        			 c=cv.first;
        			 v=cv.second;
        			 
        			 //insertion
        			 auto it = result.find(c);
								if (it != result.end()) {
									if (it->second<v) {
										result[c] = v;
										//backup[c] = v;
									}
								} else {
									result[c] = v;
									//backup[c] = v;
								}
								
								//deletion
								auto it2 = result.upper_bound(c);
								
								if (it2 != result.end() && it2->second >= v) {
						  		result.erase(c);
						  	}
						  	
						  	it = result.find(c);
								
								if (it != result.begin() && it != result.end()) {
									while(it != result.begin()) {
										it2=std::prev(it,1);
										if (it2->second <= v) {
											result.erase(it2);
										} else {break;}
									}
					  		}
        			}
        		}
        	}
        	
        	//exact update of the expectations
        	for(auto & r:result) {r.second = b.reward + r.second*model_.getDiscount();}
        	
        	assert(checkList(result));
        	return result;
        }
      
      	template <typename M>
        std::map<double,std::pair<double,std::vector<double>>> BWCPOMCP<M>::finalActionGather(ActionNode & aNode) {
        	//std::cout << "call act\n";
        	std::map<double,std::pair<double,std::vector<double>>> result;
        	
        	if(aNode.L==0){
        		std::vector<double> tmp;
        		for(auto& b:aNode.children){
        			tmp.push_back(0);
        		}
        		result[0]=std::pair<double,std::vector<double>>(aNode.V,tmp);
        		return result;
        	}
        	
        	std::vector<std::pair<double,std::map<double,double>>> children;
        	std::vector<std::map<double,double>::iterator> childrenIter;
        	for(auto & c:aNode.children) {
        		children.push_back(std::pair<double,std::map<double,double>>(c.second.proba,beliefGather(c.second)));
        		childrenIter.push_back(children.back().second.begin());
        	}
        	while(true) {
        		
        		double c = 0;
        		double v = 0;
        		std::vector<double> confidences;
        		for(int i=0; i<children.size();++i) {
        			c += children[i].first* childrenIter[i]->first;
        			v += children[i].first* childrenIter[i]->second;  
        			confidences.push_back(childrenIter[i]->first);
        		}
        		
        		auto it = result.find(c);
  					if (it != result.end()) {
  						if (it->second.first<v) {
  							result[c].first = v;
  							result[c].second = confidences;
  						}
  					} else {
  						result[c].first = v;
  						result[c].second = confidences;
  					}

						auto it2 = result.upper_bound(c);
								
						if (it2 != result.end() && it2->second.first >= v) {
				  		result.erase(c);
				  	}
				  	
				  	it = result.find(c);
				  	
				  	if (it != result.begin() && it != result.end()) {
							while(it != result.begin()) {
								it2=std::prev(it,1);
								if (it2->second.first <= v) {
									result.erase(it2);
								} else {break;}
							}
			  		}
	      		
	      		int j=0;
	      		for(j;j<childrenIter.size();++j) {
	      			++childrenIter[j];
	      			if (childrenIter[j]==children[j].second.end()) {
	      				childrenIter[j]==children[j].second.begin();
	      			} else {break;}
	      		}
	      		if (j == childrenIter.size()) {break;}
        	}
        	
        	//for(auto & r:result) {std::cout << "<" << r.first << "," << r.second << ">";}
        	//std::cout << "    act\n";
        	
        	if (result.empty()) {
        		std::cout << "******* invalid list *******\n";
		      	assert(false);
        	}
        	
        	return result;
        } 
        
        template <typename M>
        std::map<double,std::pair<double,std::pair<size_t,std::vector<double>>>> BWCPOMCP<M>::finalBeliefGather(BeliefNode & b) {
		//return type map: double -> (double,(size_t,[vector of doubles])))
		//meant as confidence -> (exp achievable with confidence),
		//(action to play,how to distribute confidences in the belief successors of the action)
        	std::map<double,std::pair<double,std::pair<size_t,std::vector<double>>>> result;
        	
        	if (valL(b) == 0) {
        		result[0].first = valV(b);
        		result[0].second.first = findBestA(b);
        		for(auto& obs:b.children[result[0].second.first].children){
        			result[0].second.second.push_back(0);
        		}
        	} else if (b.end) {
        		result[valL(b)].first = valV(b);
        	} else {
        		int i = 0;
        		size_t j=0;//number of the action
        		for (auto & a: b.children) {
							// for each action available in the root we gather a map double -> (double,[vector of doubles])
							if(a.L!=0){
        				for(auto & c:a.children) {
						  		if(c.second.proba==-1) c.second.proba = exactProba(j,b,c.first);
						  	}
        			}
        			++j;
        			std::map<double,std::pair<double,std::vector<double>>> tmp = finalActionGather(a);
        			double c,v;
        			for(auto &cv:tmp) {
        			 c=cv.first;
        			 v=cv.second.first;
        			 
        			 auto it = result.find(c);
								if (it != result.end()) {
									if (it->second.first<v) {
										result[c].first = v;
										result[c].second.first = i;
										result[c].second.second = cv.second.second;
									}
								} else {
									result[c].first = v;
									result[c].second.first = i;
									result[c].second.second = cv.second.second;
								}
								
								auto it2 = result.upper_bound(c);
								
								if (it2 != result.end() && it2->second.first >= v) {
						  		result.erase(c);
						  	}
						  	
						  	it = result.find(c);

					  		if (it != result.begin() && it != result.end()) {
									while(it != result.begin()) {
										it2=std::prev(it,1);
										if (it2->second.first <= v) {
											result.erase(it2);
										} else {break;}
									}
					  		}
        			 
        			}
        		++i;
        		}
        	}
        	
        	//for(auto & r:result) {std::cout << "<" << r.first << "," << r.second << ">";}
        	//std::cout << "   bel\n";
        	
        	if (result.empty()) {
		      	std::cout << "******* invalid list *******\n";
		      	assert(false);
        	}
        	
        	return result;
        }
        
        template <typename M> 
        bool BWCPOMCP<M>::checkList(std::map<double,double> & l) {
        	double c1,v1,c2,v2;
        	bool res = true;
        	int i=0;
        	for(auto &cv:l) {
		      	if (i!=0) {
		      		c2 = cv.first;
		      		v2 = cv.second;
		      		res = res && (c2>c1 && v2<v1);
		      	}
        		c1 = cv.first;
        		v1 = cv.second;
        		++i;
        	}
        	return res && !l.empty();
        }
        
        template <typename M> 
        std::pair<size_t,std::vector<double>> BWCPOMCP<M>::linearSolving(BeliefNode & b){
        	IloEnv env;
        	size_t curr=0;
        	try {
        		IloModel model(env);
        		IloNumVarArray vars(env);
        		IloExpr firstEx(env);
        		IloExpr confConstr(env);
        		IloExpr reward(env);
        		IloCplex cplex(model);
        		
        		size_t j=0;
        		for(auto& aNode:b.children){
        			aNode.Xa = curr; ++curr;
        			vars.add(IloNumVar(env,0.0,1.0,ILOFLOAT));
        			firstEx += vars[aNode.Xa];
        			//if(aNode.L!=0){
        				for(auto & c:aNode.children) {
						  		if(c.second.proba==-1) c.second.proba = exactProba(j,b,c.first);
						  	}
        			//}
        			++j;
        			linearSolvingExprConstruction(aNode, env, model, vars, curr, confConstr, reward, 1);
        		}
        		model.add(firstEx == 1);
        		
        		//confidence constraint
        		model.add(confConstr >= confidence_);
        		
        		//reward function
        		model.add(IloMaximize(env,reward));
        		
        		//Solver
        		if(cplex.solve()==IloFalse){
        			assert(false);
        		}
						if(cplex.getStatus() == IloAlgorithm::Optimal) {
							size_t bestA;
							double ran = distribution_(rng_);
							std::cout << "random number for strategy:" << ran << "\n"; 
							for(size_t i; i<A;++i){
								std::cout << "action: " << i << "  proba: " << cplex.getValue(vars[b.children[i].Xa]) <<"\n";
								if(ran>0 && cplex.getValue(vars[b.children[i].Xa]) != 0.0){
									bestA = i;
								}
								ran -= cplex.getValue(vars[b.children[i].Xa]); 
							}
							IloNum objval = cplex.getObjValue();
							std::cout << "objective: " << objval <<"\n";
							
							auto& aNode = b.children[bestA];
							
							std::vector<double> confidences;
							
							j=0;
							if(confidence_>0){
								for(auto& obs:aNode.children){
									auto& bel = obs.second;
									confidences.push_back(0);
									if(valL(bel)!=0){
										if(bel.end){
											assert(bel.proba != -1);
											confidences[j] += cplex.getValue(vars[aNode.Xa]) * bel.proba;
											std::cout << "Probability of selecting this action " << cplex.getValue(vars[aNode.Xa]) <<"\n";
										} else {
											for(auto& a:bel.children){
												assert(bel.proba != -1);
												confidences[j] += linearGetConfidences(a, vars, cplex)/bel.proba;
											}
										}
									} 
									assert(cplex.getValue(vars[aNode.Xa]) != 0);
									confidences[j] = confidences[j]/cplex.getValue(vars[aNode.Xa]);
									++j;
								}
							}
							env.end();
        			return std::pair<size_t,std::vector<double>>(bestA,confidences);
						} else {assert(false);}///debug
        		
        	} catch (IloException& e) {
						std::cerr << "Cplex exception: " << e << std::endl;
					} catch (...) {
						std::cerr << "Unknown Exception" << std::endl;
					}
        }
        
        template <typename M> 
        void BWCPOMCP<M>::linearSolvingExprConstruction(ActionNode & aNode, IloEnv& env, IloModel& model, IloNumVarArray& vars, size_t& curr, IloExpr& confConstr, IloExpr& reward, double discount){
        	if(aNode.L==0){
        		reward += discount * vars[aNode.Xa] * aNode.V;
        	} else {
		      	for(auto& ob:aNode.children){
		      		auto& b = ob.second;
                    assert(b.proba != -1);
		      		if(valL(b)==0){
		      			reward += discount * vars[aNode.Xa] * b.proba * (valV(b)+b.reward);
		      		} else if (b.end) { //?
		      			if(b.rem<=0.0) {
		      				confConstr += vars[aNode.Xa] * b.proba;
		      			}
		      			reward += discount * vars[aNode.Xa] * b.proba * (valV(b)+b.reward);
		      		} else {
		      			IloExpr expr(env);
		      			reward += discount * vars[aNode.Xa] * b.proba * b.reward;
		      			assert(b.children.size()==A);
		      			size_t j=0;
		      			for(auto& a:b.children){
		      				a.Xa = curr; ++curr;
		      				vars.add(IloNumVar(env,0.0,1.0,ILOFLOAT));
		      				expr += vars[a.Xa];
		      				//if(a.L!=0){
				    				for(auto & c:a.children) {
											if(c.second.proba==-1) c.second.proba = exactProba(j,b,c.first);
										}
				    		//	}
				    		++j;
		      				linearSolvingExprConstruction(a, env, model, vars, curr, confConstr, reward, discount * model_.getDiscount());
		      			}
		      			model.add(expr == vars[aNode.Xa] * b.proba);
		      		}
		      	}
		      }
        }
        
        
        template <typename M> 
        double BWCPOMCP<M>::linearGetConfidences(ActionNode & aNode, IloNumVarArray& vars, IloCplex& cplex){
        	double res = 0;
        	for(auto& ob:aNode.children){
        		auto& b = ob.second;
	      		if(valL(b)!=0){
	      			if(b.end){
	      				assert(valL(b) >= 1.0 - CEPS || valL(b)<= CEPS);
	      				assert(b.proba != -1);
	      				res += cplex.getValue(vars[aNode.Xa]) * b.proba;
	      			} else {
	      				for(auto& a:b.children){
	      					res += linearGetConfidences(a, vars, cplex);
	      				}
	      			}
	      		}
        	}
        	return res;
        }
        
    }
} //a

#endif
